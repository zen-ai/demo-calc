import { IGrid_Item } from "../../molecules";

export const row_1: IGrid_Item[] = [
  { toggle: true, value: "AC" },
  { toggle: false, value: null },
  { toggle: false, value: null },
  { toggle: true, value: "÷" },
]; 

export const row_2: IGrid_Item[] = [
  { toggle: false, value: "7" },
  { toggle: false, value: "8" },
  { toggle: false, value: "9" },
  { toggle: true, value: "*" },
];

export const row_3: IGrid_Item[] = [
  { toggle: false, value: "4" },
  { toggle: false, value: "5" },
  { toggle: false, value: "6" },
  { toggle: true, value: "-" },
];

export const row_4: IGrid_Item[] = [
  { toggle: false, value: "1" },
  { toggle: false, value: "2" },
  { toggle: false, value: "3" },
  { toggle: true, value: "+" },
];

export const row_5: IGrid_Item[] = [
  { toggle: false, value: "0" },
  { toggle: false, value: "." },
  { toggle: false, value: null },
  { toggle: true, value: '=' },
];


const CalcGridStaticData = [
  row_1,
  row_2,
  row_3,
  row_4,
  row_5
]

type ICalcGridStaticData = typeof CalcGridStaticData;

export type { ICalcGridStaticData };
export { CalcGridStaticData };