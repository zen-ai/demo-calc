import React, { FC } from "react";
import { CalculatorStyles } from "./StyledCalculator";
import { ICalculator } from "./ICalculator";
import { UserInputGrid, DisplayInfoSection } from "../../molecules";

const Calculator: FC<ICalculator> = () => {
  return (
    <CalculatorStyles>
      <DisplayInfoSection/>
      <UserInputGrid />
    </CalculatorStyles>
  );
};

export { Calculator };
