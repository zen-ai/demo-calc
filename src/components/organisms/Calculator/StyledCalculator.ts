import styled from "styled-components";
import { theme } from "../../shared";

interface ICalculatorStyles {}

const {
  colors: { primary_dark },
} = theme;

const CalculatorStyles = styled.div<ICalculatorStyles>`
  margin: 0 auto;
  width: 25rem;
  background-color: ${primary_dark};
  border: 1px solid black;
  height: 48rem;
  border-radius: 20px;
  box-shadow: 5px 5px 12px #8c8c8c;

  .display-info-section,
  .user-input-section {
    padding: 2rem;

    p {
      margin: 0;
      text-align: right;
    }
  }

  .border {
    width: 17rem;
    margin-left: auto;
    height: 2px;
    background-color: grey;
  }
`;

export { CalculatorStyles };
