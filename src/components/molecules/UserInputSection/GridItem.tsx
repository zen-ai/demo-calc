import React, { FC } from "react";
import { IGrid_Item } from "./IUserInputSection";
import { CalculatorSliceActions } from "../../store/slices/CalculatorSlice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { Add, GetValues, Subtract, Multiply, Divide } from "./helpers"

interface IUserInputGridItem {
  item: IGrid_Item;
}

const isValidItem = (item: IGrid_Item) => {
  const { value, toggle } = item;
  return value === null && toggle === false;
};

// Row Item
const GridItem: FC<IUserInputGridItem> = ({
  item,
  item: { value, toggle: hasDarkBackground },
}: IUserInputGridItem) => {
  const { updateEquationValue, allClear, updateResultsValue, updateError } = CalculatorSliceActions;

  const dispatch = useAppDispatch();
  const { result, equation } = useAppSelector(
    ({ calculatorSlice }) => calculatorSlice
  );

  const HandleOnClick = () => {

    // User presses "AC"
    if (value && value === "AC") {
      dispatch(allClear());
      dispatch(updateError({ msg: '', hasError: false }));
      return;
    };

    /*
      TODO - Current implementation is brute force.
    */
    if (value && value === "=") {
      // Run Inital Calulation
      if (result === "0") {
        const { _op, value_2, value_1, error } = GetValues({ equation });

        if (error.hasError) {
          dispatch(updateError({ hasError: true, msg: 'Syntax Error' }))
          return;
        }
        

        switch (_op) {
          case '+': {
            const { value } = Add({ value_1, value_2 })
            value && dispatch(updateResultsValue({ value: String(value) }));
            break;
          }
          case '-': {
            const { value } = Subtract({ value_1, value_2 })
            value && dispatch(updateResultsValue({ value: String(value) }));
            break;
          }
          case '÷': {
            const { value } = Divide({ value_1, value_2 })
            value && dispatch(updateResultsValue({ value: String(value) }));
            break;
          }
          case '*': {
            const { value } = Multiply({ value_1, value_2 })
            value && dispatch(updateResultsValue({ value: String(value) }));
            break;
          }
          default: {
            break;
          }
        }
      } else {
        // continue Calculation
      }
      return;
    };

    value && dispatch(updateEquationValue({ value }));
  };

  return (
    <div
      onClick={HandleOnClick}
      className={`row-item ${!hasDarkBackground ? " dark-background" : ""} ${isValidItem(item) ? " no-background" : ""
        } `}
    >
      {value && value}
    </div>
  );
};

export { GridItem };
