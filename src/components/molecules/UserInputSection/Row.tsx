import React, { FC } from "react";
import { IGrid_Item, IRow } from "./IUserInputSection";
import { GridItem } from "./GridItem"

const Row: FC<IRow> = ({ row }: IRow) => (
  <div className="row">
    {row.map((item: IGrid_Item, i: number) => {
      const { value } = item;
      return <GridItem key={`${i}_${value}`} item={item} />;
    })}
  </div>
);


export { Row };
