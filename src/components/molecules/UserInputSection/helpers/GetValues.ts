interface IGetValues {
  equation: string;
}

const _operators = ["+", "-", "*", "÷"];

const scanString = (str: string[], checkArr: any[]) => {
  let scan = str;

  scan.forEach((val, i) => {
    if (_operators.includes(val)) {
      if (scan[i] === "-") {
        // Check if first integer is negative or more consecutive operators
        if (scan[i - 1] === undefined || isNaN(parseInt(scan[i - 1]))) {
          return;
        }
      }

      checkArr.push({ val, i });
    }
  });

  return { checkArr };
};

const checkNegatives = (str: string) => {
  return str.split("").filter((num) => _operators.includes(num));
};

const GetValues = ({ equation }: IGetValues) => {
  let inputOps: { val: string; i: number }[] = [];
  const error = { hasError: false };
  const scan = equation.split("");

  if (scan.length <= 2) {
    error.hasError = true;

    return {
      error,
      _op: "",
      value_1: "",
      value_2: "",
    };
  }

  const { checkArr } = scanString(scan, inputOps);

  inputOps = checkArr;

  const diff = scan.length - inputOps.length;

  // Check if there are more operators than numbers, turn error flag on if true
  if (inputOps.length > diff && inputOps.length >= 3) {
    error.hasError = true;
    return {
      error,
      _op: "",
      value_1: "",
      value_2: "",
    };
  }

  const userInputOp = inputOps.length > 0 ? inputOps[0].i : null;
  
  if (userInputOp) {
    const _op = scan[userInputOp],
      value_1 = equation.slice(0, inputOps[0].i),
      value_2 = equation.slice(inputOps[0].i + 1);

    if (
      !value_1 ||
      !value_2 ||
      checkNegatives(value_1).length >= 2 ||
      checkNegatives(value_2).length >= 2
    ) {
      error.hasError = true;
      return {
        error,
        _op: "",
        value_1: "",
        value_2: "",
      };
    }

    return {
      _op,
      value_1,
      value_2,
      error,
    };
  } else {
    return {
      _op: '',
      value_1: '',
      value_2: '',
      error: { hasError: true },
    };
  }
};

export { GetValues };
