import { IActiveCalac } from ".."

const Add = ({ value_1, value_2 }: IActiveCalac): { value: number } => {

  let value = parseFloat(value_1) + parseFloat(value_2)
 
  return {
    value
  }

}; 

export { Add };
