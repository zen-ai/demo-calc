import React, { FC } from "react";
import { StyledUserInputSection } from "./StyledUserInputSection";
import { Row_Type } from "./IUserInputSection";
import { useAppSelector } from "../../store/hooks";
import { Row } from "./Row"

const UserInputGrid: FC = () => {
  const { inputGrid: grid } = useAppSelector(
    ({ calculatorSlice }) => calculatorSlice
  );
  return (
    <StyledUserInputSection>
      {grid.map((row: Row_Type, i: number) => {
        return <Row row={row} key={i} />;
      })}
    </StyledUserInputSection>
  );
};

export { UserInputGrid };
