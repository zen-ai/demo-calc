type HeaderTypes = "h1" | "h2" | "h3" | "h4" | "h5" | "h6";
type ElementTypes = "span" | "p";
type BoldTypes = "100" | "200" | "300" | "400" | "500" | "600" | "700" | "800";

interface IIcon {
  width: string;
}

interface ISVGIcon extends IIcon {
  src: string;
  label: string;
}

interface ILineDivider {
  width?: string;
  padding?: string;
  margin?: string;
  height?: string;
}

interface IStyledElement {
  fontSize?: string;
  fontWeight?: string;
  color?: string;
  letterSpacing?: string;
  margin?: string;
  padding?: string;
}
interface IStyledButtonProps {
  textColor: string;
  padding?: string;
  backgroundColor: string;
  fontSize: string;
  textAlign?: string;
  display?: string;
  width?: string;
  margin?: string;
  border?: string;
}

interface IElement {
  text: string;
  type: ElementTypes;
}

interface IButton extends IStyledButtonProps {
  text: string;
  onClick: () => void;
}

interface IHeaders {
  h1?: boolean;
  h2?: boolean;
  h3?: boolean;
  h4?: boolean;
  h5?: boolean;
  h6?: boolean;
}

interface IHeader extends IHeaders, IStyledElement {
  text: string;
  type: HeaderTypes;
}

interface IStyledParagraphElement extends IStyledElement {}
interface IStyledSpanElement extends IStyledElement {}

export type {
  IIcon,
  ElementTypes,
  BoldTypes,
  IStyledElement,
  IElement,
  IStyledParagraphElement,
  IStyledSpanElement,
  IHeader,
  IButton,
  ISVGIcon,
  IStyledButtonProps,
  ILineDivider,
};
