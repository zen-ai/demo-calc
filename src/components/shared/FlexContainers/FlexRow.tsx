import React, { FC, ReactChild, ReactChildren } from "react";
import { GenericFlexContainer } from "./GenericFlexContainer";

interface IFlex {
  children: ReactChild | ReactChildren;
  margin: string;
  padding?: string;
  width?: string;
  justifyContent?: string;
}

const FlexRowDiv: FC<IFlex> = ({
  children,
  width = "auto",
  padding = "0",
  margin = "0",
  justifyContent = "flex-start",
}: IFlex) => {
  return (
    <GenericFlexContainer
      width={width}
      flexDisplay={"flex"}
      flexDirection={"row"}
      justifyContent={justifyContent}
      margin={margin}
      padding={padding}
    >
      {children}
    </GenericFlexContainer>
  );
};

export { FlexRowDiv };
export type { IFlex };
