import styled from "styled-components";

interface IFlexContainer {
  flexDisplay: string;
  justifyContent: string;
  flexDirection?: string;
  padding?: string;
  margin?: string;
  width?: string;
}
const GenericFlexContainer = styled.div<IFlexContainer>`
  display: ${({ flexDisplay }: IFlexContainer) => flexDisplay};
  justify-content: ${({ justifyContent }: IFlexContainer) => justifyContent};
  padding: ${({ padding = "0" }: IFlexContainer) => padding};
  margin: ${({ margin = "0" }: IFlexContainer) => margin};
  width: ${({ width = "auto" }: IFlexContainer) => width};
  flex-direction: ${({ flexDirection = "row" }: IFlexContainer) => flexDirection};
`;

export { GenericFlexContainer };
