import React, { FC } from "react";
import { GenericFlexContainer } from "./GenericFlexContainer";
import { IFlex } from "./FlexRow";

const FlexColumnDiv: FC<IFlex> = ({
  children,
  width = "auto",
  padding = "0",
  margin = "0",
  justifyContent = "flex-start",
}: IFlex) => {
  return (
    <GenericFlexContainer
      width={width}
      flexDisplay={"flex"}
      flexDirection={"column"}
      justifyContent={justifyContent}
      margin={margin}
      padding={padding}
    >
      {children}
    </GenericFlexContainer>
  );
};

export { FlexColumnDiv };
