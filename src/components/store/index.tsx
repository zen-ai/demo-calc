import { configureStore } from "@reduxjs/toolkit";
import calculatorSlice from "./slices/CalculatorSlice";
/**
  Define Root State and Dispatch Types

  Using configureStore should not need any additional typings. 
  You will, however, want to extract the RootState type and the Dispatch type so 
  that they can be referenced as needed. Inferring these types from the store itself 
  means that they correctly update as you add more state slices or modify middleware settings.

  More info at https://redux-toolkit.js.org/usage/usage-with-typescript;

  Middlewares
  More infot at https://redux-toolkit.js.org/api/getDefaultMiddleware
 
  Setting serializableCheck to false to overide development middlewares
*/

const store = configureStore({
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
  reducer: { calculatorSlice },
});

/**
  Infer the `RootState` and `AppDispatch` types from the store itself
*/

type IRootState = ReturnType<typeof store.getState>;
type IAppDispatch = typeof store.dispatch;

export type { IRootState };
export type { IAppDispatch };
export { store };
