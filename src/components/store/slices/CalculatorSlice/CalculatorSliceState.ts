import { ICalculatorState } from "./ICalculatorSlice";
import { CalcGridStaticData } from "../../../organisms/Calculator/StaticValues"

const name = "Calculator";

const initialState: ICalculatorState = {
    equation: '0',
    result: '0',
    inputGrid: CalcGridStaticData,
    errorState: null
};

export { name, initialState };
