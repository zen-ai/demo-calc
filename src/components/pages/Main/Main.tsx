import React from "react";
import { StyledMain } from "./StyledMain";
import { Calculator } from "../../organisms";

const Main = () => (
  <StyledMain>
    <h1 style={{ textAlign: 'center', marginBottom:'2rem'}} >Calculator</h1>
    <Calculator />
    <code style={{ textAlign: 'center', marginTop:'1rem'}}>by John Tenezaca</code>
  </StyledMain>
);

export { Main };
