import styled from "styled-components";
import { theme } from "../../shared";

interface IStyled_Main {}

const {
  colors: { primary_dark, primary_green },
} = theme;

const StyledMain = styled.div<IStyled_Main>`
  display: flex;
  flex-direction: column;
  max-width: 500px;
  margin: 3rem auto 0;

  .calc {
    margin: 0 auto;
    width: 25rem;
    background-color: ${ primary_dark };
    border: 1px solid black;
    height: 45rem;
    border-radius: 20px;

    .display-info-section,
    .user-input-section  {
      padding: 2rem;

      p {
        margin: 0;
        text-align: right;
      }
    }

    .display-info-section {
      .calculation-section {
        margin: 1rem 0 0;
        p {
          color: white;
          font-size: 22px
        }
      }
      .results-section {
        margin-bottom: 1rem;
        p {
          font-size: 80px;
          font-weight: bold;
          color: ${ primary_green }
        }
      }

      .border {
        width: 17rem;
        margin-left: auto;
        height: 2px;
        background-color: grey;
      }
    }

    .user-input-section {
      padding-top: 0%;

      * {
        color: white;
      }

      .row {
        display: flex;

        .row-item {
          display: flex;
          justify-content: center;
          align-items: center;
          margin: auto;
          height: 50px;
          width: 60px;
          background-color: #444444;
         border-radius: 10px;
        }

        .row-item:last-child {
          margin-right: 0;
        }
        
      }
    }


  }
`;

export { StyledMain };
