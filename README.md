# Vidmob Take Home

## Features
    - Whole Numbers
    - Integers 
    - Negtive Numbers
    - Decimals
    - Error checking
    
## TODOs
    - Unit Tests
    - Multiple operators
    - Nested parentheses

## Resources
- [Design](https://dribbble.com/shots/11027089-004-calculator)
- Codebase inspired by atomic design

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

